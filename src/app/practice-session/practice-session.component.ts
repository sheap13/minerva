import { Component, OnInit } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

import { Exercise } from '../exercise/exercise';
import { ExerciseComponent } from '../exercise/exercise.component';
import { ExerciseService } from '../exercise/exercise.service';

import * as moment from 'moment';
import 'moment-duration-format';

@Component({
    selector: 'minerva-practice-session',
    templateUrl: 'practice-session.component.html',
    styleUrls: [
    ],
})

export class PracticeSessionComponent implements OnInit {
    public endTime: moment.Moment;
    public timeRemaining: moment.Duration;
    public sessionTime: moment.Duration;
    public mode: string;
    public exercise: string;
    public warmupExercises: Exercise[];
    public sessionProgress = 0;
    public phaseProgress = 0;
    public warmupTime: moment.Duration;
    public lessonTime: moment.Duration;
    private calculatePercentElapsed = (remaining: moment.Duration, total: moment.Duration) => {
        return 100 - (remaining.as('seconds') / total.as('seconds')) * 100;
    }

    constructor(
        private exerciseService: ExerciseService
    ) {}

    public ngOnInit(): void {
        this.mode = 'warmup';
        this.exerciseService.getExercises()
            .then((result) => {
                this.warmupExercises = result;
                this.exercise = this.warmupExercises[0].id;
            });
        this.warmupTime = moment.duration(5, 'minutes');
        this.lessonTime = moment.duration(25, 'minutes');
        this.sessionTime = moment.duration(30, 'minutes');
        this.endTime = moment().add(this.sessionTime);
        let now = moment();
        this.timeRemaining = moment.duration(this.endTime.diff(now));
        setInterval(() => {
            now = moment();
            this.timeRemaining = moment.duration(this.endTime.diff(now));
            this.sessionProgress = this.calculatePercentElapsed(this.timeRemaining, this.sessionTime);
            if (this.timeRemaining < this.lessonTime) {
                this.mode = 'lesson';
                this.phaseProgress = this.calculatePercentElapsed(this.timeRemaining, this.lessonTime);
            } else {
                this.phaseProgress = 100 - ((this.timeRemaining.as('seconds') - this.lessonTime.as('seconds')) / this.warmupTime.as('seconds')) * 100;
            }
        }, 100);
    }
}
