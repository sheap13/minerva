import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TitleCasePipe } from '@angular/common';
import { MatProgressBarModule } from '@angular/material';

import { ScoreDirective } from '../score/score.directive';
import { Exercise } from '../exercise/exercise';
import { ExerciseService } from '../exercise/exercise.service';
import { ExerciseComponent } from '../exercise/exercise.component';

import { PracticeSessionComponent } from './practice-session.component';
import * as moment from 'moment';

describe('Practice session', () => {
    let fixture: ComponentFixture<PracticeSessionComponent>;
    let practiceSession: PracticeSessionComponent;
    const testExercises = [
        {
            id: 'cMajScale',
        },
    ];
    const mockExerciseService = {
        getExercises: () => {
            return {
                then: (callback) => {
                    callback(testExercises);
                }
            };
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                MatProgressBarModule,
            ],
            declarations: [
                ExerciseComponent,
                ScoreDirective,
                PracticeSessionComponent,
            ],
            providers: [
                {
                    provide: ExerciseService,
                    useValue: mockExerciseService,
                },
            ],
        })
        .compileComponents();

        fixture = TestBed.createComponent(PracticeSessionComponent);
        practiceSession = fixture.componentInstance;

        jasmine.clock().install();
        jasmine.clock().mockDate(new Date(2000, 1, 1));

        practiceSession.ngOnInit();
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });

    it('should initialise the clock with session time', () => {
        expect(practiceSession.timeRemaining.as('seconds')).toBe(practiceSession.sessionTime.as('seconds'));
    });

    it('should count down with time', () => {
        jasmine.clock().tick(60000);
        expect(practiceSession.timeRemaining.as('seconds')).toBe(practiceSession.sessionTime.as('seconds') - 60);
    });

    it('should start with a warmup', () => {
        expect(practiceSession.mode).toBe('warmup');
    });

    it('should present first exercise during warmup', () => {
        expect(practiceSession.exercise).toBe(testExercises[0].id);
    });

    it('should switch to lesson practice after 5 minutes', () => {
        jasmine.clock().tick(5 * 60 * 1000 + 100);
        expect(practiceSession.mode).toBe('lesson');
    });

    it('should correctly calculate percentage of session expired', () => {
        expect(practiceSession.sessionProgress).toBe(0);
        jasmine.clock().tick(3 * 60 * 1000);
        expect(practiceSession.sessionProgress).toBe(10);
    });

    it('should correctly calculate percentage of phase expired', () => {
        expect(practiceSession.phaseProgress).toBe(0);
        jasmine.clock().tick(1 * 60 * 1000);
        expect(practiceSession.phaseProgress).toBe(20);
        jasmine.clock().tick(5 * 60 * 1000);
        expect(practiceSession.phaseProgress).toBe(4);
    });
});
