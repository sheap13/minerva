import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { OSMD } from 'opensheetmusicdisplay';

@Directive({
    selector: '[minervaMyScore]',
})

export class ScoreDirective implements OnInit {
    @Input('scorePath') private scorePath: string;
    private osmd: OSMD;

    constructor(private el: ElementRef) {}

    public ngOnInit(): void {
        console.log(this.scorePath);
        if (this.scorePath) {
            this.osmd = new OSMD(this.el.nativeElement, true);
            this.osmd.load(this.scorePath)
                .then(
                    () => this.osmd.render(),
                    (err: any) => console.log(err),
                );
        }
    }
}
