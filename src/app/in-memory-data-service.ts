import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
    public createDb() {
        const users = [
            {
                email: 'sheap13@gmail.com',
                username: 'Sheap',
            },
        ];
        const exercises = [
            {
                id: 'cMajScale',
                label: 'C Major Scale',
                sections: [
                    {
                        type: 'score',
                        score: 'assets/exercises/cMajScale/scale.xml',
                    },
                ],
            },
            {
                id: 'hanon_01',
                label: 'Hanon Exercise 01',
                sections: [
                    {
                        type: 'heading',
                        text: 'The Virtuoso Pianist Exercise 1',
                    },
                    {
                        type: 'text',
                        text: 'Begin with your hands one octave apart, with your left hand pinky on the'
                            + ' C two octaves below middle C',
                    },
                    {
                        type: 'score',
                        score: 'assets/exercises/Hanon/hanon_01a.xml',
                    },
                    {
                        type: 'text',
                        text: 'Continue with the above ascending pattern for 2 octaves until your hands'
                            + ' reach a position ready to start the below descending pattern (left pinky'
                            + ' should get to middle C)',
                    },
                    {
                        type: 'score',
                        score: 'assets/exercises/Hanon/hanon_01b.xml',
                    },
                ],
            },
        ];
        return {
            users,
            exercises,
        };
    }
}
