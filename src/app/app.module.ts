import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {
    MatCardModule, // tslint:disable-line no-unused-variable
    MatButtonModule, // tslint:disable-line no-unused-variable
    MatInputModule, // tslint:disable-line no-unused-variable
    MatToolbarModule, // tslint:disable-line no-unused-variable
    MatSnackBarModule,
    MatProgressBarModule,
} from '@angular/material';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleCasePipe } from '@angular/common';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data-service';

import { ExerciseComponent } from './exercise/exercise.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { PracticeSessionComponent } from './practice-session/practice-session.component';
import { RegisterComponent } from './register/register.component';
import { ScoreDirective } from './score/score.directive';

import { NotFoundComponent } from './not-found/not-found.component';

import { ExerciseService } from './exercise/exercise.service';
import { UserService } from './user/user.service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        AppRoutingModule,
        HttpModule,
        MatCardModule,
        MatButtonModule,
        MatInputModule,
        MatToolbarModule,
        MatSnackBarModule,
        MatProgressBarModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        LoginComponent,
        RegisterComponent,
        LandingComponent,
        HomeComponent,
        ExerciseComponent,
        ScoreDirective,
        NotFoundComponent,
        PracticeSessionComponent,
    ],
    providers: [
        UserService,
        ExerciseService,
    ],
    bootstrap: [
        AppComponent,
    ],
})

export class AppModule { }
