import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { LandingComponent } from './landing.component';

describe('Landing Component', () => {
    let comp: LandingComponent;
    let fixture: ComponentFixture<LandingComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
            ],
            declarations: [
                LandingComponent,
            ],
            providers: [
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LandingComponent);
        comp = fixture.componentInstance;
    });

    it('should display heading', () => {
        const headingDe: DebugElement = fixture.debugElement.query(By.css('h1'));
        const headingEl: HTMLElement = headingDe.nativeElement;

        expect(headingEl.textContent).toContain('Welcome to Minerva');
    });
});
