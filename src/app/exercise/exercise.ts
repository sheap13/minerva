import { ExerciseSection } from './exercise-section';

export class Exercise {
    public id: string;
    public label: string;
    private sections: ExerciseSection[];
}
