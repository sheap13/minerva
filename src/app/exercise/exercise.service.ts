import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Exercise } from './exercise';

@Injectable()
export class ExerciseService {
    private exercisesUrl = 'api/exercises';

    constructor(private http: Http) {}

    public getExercises(): Promise<Exercise[]> {
        return this.http.get(this.exercisesUrl)
            .toPromise()
            .then((response) => response.json().data as Exercise[])
            .catch(this.handleError);
    }

    public getExercise(id: string): Promise<Exercise> {
        const url = `${this.exercisesUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then((response) => response.json().data as Exercise)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }
}
