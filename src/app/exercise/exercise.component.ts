import { Component, Input, OnChanges } from '@angular/core';

import { Exercise } from './exercise';
import { ExerciseService } from './exercise.service';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'minerva-exercise',
    templateUrl: 'exercise.component.html',
    styleUrls: [
        'exercise.component.css',
    ],
})

export class ExerciseComponent implements OnChanges {
    @Input() exerciseName: string;
    private exercise: Exercise;

    constructor(
        private exerciseService: ExerciseService,
    ) {}

    public ngOnChanges(): void {
        if (this.exerciseName) {
            this.exerciseService.getExercise(this.exerciseName)
                .then((exercise) => {
                    this.exercise = exercise;
                });
        }
    }
}
