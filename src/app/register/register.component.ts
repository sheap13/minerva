import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'minerva-register',
    templateUrl: 'register.component.html',
    styleUrls: [
        '../generic-styles/small-forms.css',
    ],
})

export class RegisterComponent {
    constructor(
        private router: Router,
        private snackbar: MatSnackBar,
    ) {}

    public register(): void {
        this.snackbar.open(
            'Thank you for your application, an email has been sent to you to complete the registration process.',
            undefined,
            {
                duration: 2000,
            },
        );
        this.router.navigate(['/landing']);
    }
}
