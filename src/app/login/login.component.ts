import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

@Component({
    selector: 'minerva-login',
    templateUrl: 'login.component.html',
    styleUrls: [
        '../generic-styles/small-forms.css',
    ],
})

export class LoginComponent {
    private email: string;

    constructor(
        private userService: UserService,
        private router: Router,
        private snackbar: MatSnackBar,
    ) {}

    public addMessageAndLeave(user: User): void {
        this.snackbar.open('Welcome ' + user.username,
                           undefined,
                           {
                               duration: 2000,
                           });
        this.router.navigate(['/home']);
    }

    public login(): void {
        this.userService.login(this.email, this.addMessageAndLeave.bind(this));
    }
}
