import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { User } from '../user/user';
import { UserService } from '../user/user.service';

import { LoginComponent } from './login.component';

describe('Login Component', () => {
    let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    const mockMessengerService: any = {
        addMessage: (username: string) => {
            return;
        }
    };
    const mockUserService: any = {
        login: (username: string) => {
            return;
        },
    };
    let debugElement: any;
    let loginSpy: any;
    let loginService: any;
    let emailDe: DebugElement;
    let emailEl: HTMLInputElement;
    let navigateSpy: any;
    const testUser: User = {
        email: 'test@test.com',
        username: 'testUser',
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                FormsModule,
                MatCardModule,
                MatButtonModule,
                MatInputModule,
                MatSnackBarModule,
            ],
            declarations: [
                LoginComponent,
            ],
            providers: [
                {
                    provide: UserService,
                    useValue: mockUserService,
                },
            ],
        })
        .compileComponents();

        fixture = TestBed.createComponent(LoginComponent);
        comp = fixture.componentInstance;

        debugElement = fixture.debugElement;

        loginService = debugElement.injector.get(UserService);
        loginSpy = spyOn(loginService, 'login');

        emailDe = debugElement.query(By.css('#email'));
        emailEl = emailDe.nativeElement;

        navigateSpy = spyOn((<any>comp).router, 'navigate');
    }));

    it('should display heading', () => {
        const headingDe: DebugElement = debugElement.query(By.css('mat-card-title'));
        const headingEl: HTMLElement = headingDe.nativeElement;

        expect(headingEl.textContent).toContain('Login');
    });

    it('should display email field', () => {
        expect(emailEl.attributes['placeholder'].value).toBe('Email Address');
        expect(emailEl.attributes['type'].value).toBe('email');
    });

    it('should call user.login with email as parameter', async(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            emailEl.value = 'test@test.com';
            emailEl.dispatchEvent(new Event('input'));
            fixture.detectChanges();

            comp.login();
            // Direct comparison of function seems to be flummoxed by this binding
            expect(loginSpy).toHaveBeenCalledWith('test@test.com', jasmine.any(Function));
        });
    }));

    it('should redirect to landing page', function() {
        comp.addMessageAndLeave(testUser);
        expect(navigateSpy).toHaveBeenCalledWith(['/home']);
    });
});
