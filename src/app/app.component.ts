import { Component } from '@angular/core';

@Component({
    selector: 'minerva-my-app',
    templateUrl: 'app.component.html',
    styleUrls: [
        'app.component.css',
    ],
})

export class AppComponent {
}
