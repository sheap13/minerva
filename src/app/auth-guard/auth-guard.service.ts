import { Injectable } from '@angular/core';
import {
    CanActivate,
    Router,
} from '@angular/router';

import { UserService } from '../user/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private userService: UserService, private router: Router) {}

    public canActivate() {
        if (this.userService.loggedIn) {
            return true;
        } else {
            this.router.navigate(['/landing']);
            return false;
        }
    }
}
