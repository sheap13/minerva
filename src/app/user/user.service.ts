import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class UserService {
    public loggedIn = false;
    public user: User;
    private usersUrl = 'api/users';

    constructor(
        private http: Http,
    ) { }

    public handleResponse(response: any, emailRequest: string, callback: (user: User) => any): void {
        return response._body.data.forEach((user: User) => {
            if (user.email === emailRequest) {
                this.user = user;
                this.loggedIn = true;
                callback(this.user);
            }
        });
    }

    public login(emailRequest: string, callback: (user: User) => any): Promise<void> {
        const url = `${this.usersUrl}`;

        return this.http.get(url)
            .toPromise()
            .then((response) => this.handleResponse(response, emailRequest, callback));
    }
}
