import { Component, OnInit } from '@angular/core';

import { Exercise } from '../exercise/exercise';
import { ExerciseService } from '../exercise/exercise.service';

@Component({
    selector: 'minerva-home',
    templateUrl: 'home.component.html',
    styleUrls: [
    ],
})

export class HomeComponent implements OnInit {
    public exercises: Exercise[] = [];

    constructor(private exerciseService: ExerciseService) {}

    public ngOnInit(): void {
        this.exerciseService.getExercises()
            .then((result) => {
                this.exercises = result;
            });
    }
}
